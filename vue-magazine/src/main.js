import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App.vue';
import router from './router';
import tinymce from 'vue-tinymce-editor';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { useFilters } from '@/helpers/filters.helpers';
import { useAxiosCustomInterceptor } from '@/helpers/interceptor.helpers';

Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);
Vue.config.productionTip = false;
Vue.component('tinymce', tinymce);

useFilters();
useAxiosCustomInterceptor();

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
