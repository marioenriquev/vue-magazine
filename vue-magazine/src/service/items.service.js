import { config_settings } from '@/config.js';
import Vue from 'vue';

const baseApiUrl = config_settings.RESOURCE_SERVER_URL;

export const getItemsxPage = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/page/' + id + '/items',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateItemPhoto = (jsonObject, token) => {
  return Vue.$http({
    method: 'put',
    url: baseApiUrl + '/PUT/items/default_photo',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const uploadPhotoxItem = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items/' + id + '/photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const uploadSecondPhotoxItem = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items/' + id + '/second_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const uploadThirdPhotoxItem = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items/' + id + '/third_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const addNewItem = (jsonObject, token) => {
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateIndividualItem = (jsonObject, token) => {
  return Vue.$http({
    method: 'put',
    url: baseApiUrl + '/PUT/items',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteItemsxPage = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/page/' + id + '/items',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteIndividualItem = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/item/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};
