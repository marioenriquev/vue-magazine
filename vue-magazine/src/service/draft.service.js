import { config_settings } from '@/config.js';
import Vue from 'vue';

const baseApiUrl = config_settings.RESOURCE_SERVER_URL;

export const getDraftsxPage = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/pages/' + id + '/drafts',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const getIndividualDraft = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/drafts/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateBackgroundxDraft = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts/' + id + '/background',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateFirstPhotoxDraft = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts/' + id + '/photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateSecondPhotoxDraft = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts/' + id + '/second_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const addIndividualDraft = (jsonObject, token) => {
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteIndividualDraft = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/drafts/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};
