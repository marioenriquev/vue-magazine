import { config_settings } from '@/config.js';
import Vue from 'vue';

const baseApiUrl = config_settings.RESOURCE_SERVER_URL;
//pages
export const getAdminPages = (token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/pages?cReg=90',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const getPublicPages = () => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/pages/only_enabled',
  });
};

export const getIndividualPage = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/pages/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const getUpdatePageDependencies = (token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/pages/form_data',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateIndividualPage = (jsonObject, token) => {
  return Vue.$http({
    method: 'put',
    url: baseApiUrl + '/PUT/pages',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const addIndividualPage = (jsonObject, token) => {
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/pages',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateBackgroundxPage = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/pages/' + id + '/background',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateFirstPhotoxPage = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/pages/' + id + '/photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateSecondPhotoxPage = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/pages/' + id + '/second_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteIndividualPage = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/pages/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const setPagesOrder = (checkedNames, token) => {
  return Vue.$http({
    method: 'put',
    url: baseApiUrl + '/PUT/pages/orders',
    data: checkedNames,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

//drafts
export const getDraftsxPage = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/pages/' + id + '/drafts',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const getIndividualDraft = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/drafts/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateBackgroundxDraft = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts/' + id + '/background',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateFirstPhotoxDraft = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts/' + id + '/photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateSecondPhotoxDraft = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts/' + id + '/second_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const addIndividualDraft = (jsonObject, token) => {
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/drafts',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteIndividualDraft = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/drafts/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

//items
export const getItemsxPage = (id, token) => {
  return Vue.$http({
    method: 'get',
    url: baseApiUrl + '/GET/page/' + id + '/items',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateItemPhoto = (jsonObject, token) => {
  return Vue.$http({
    method: 'put',
    url: baseApiUrl + '/PUT/items/default_photo',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const uploadPhotoxItem = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items/' + id + '/photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const uploadSecondPhotoxItem = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items/' + id + '/second_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const uploadThirdPhotoxItem = (id, imageData, token) => {
  var form = new FormData();
  form.append('file', imageData);
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items/' + id + '/third_photo',
    data: form,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const addNewItem = (jsonObject, token) => {
  return Vue.$http({
    method: 'post',
    url: baseApiUrl + '/POST/items',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const updateIndividualItem = (jsonObject, token) => {
  return Vue.$http({
    method: 'put',
    url: baseApiUrl + '/PUT/items',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteItemsxPage = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/page/' + id + '/items',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteIndividualItem = (id, token) => {
  return Vue.$http({
    method: 'delete',
    url: baseApiUrl + '/DELETE/item/' + id,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};
