import { config_settings } from '@/config.js';
import Vue from 'vue';

export const postLogin = (body_credentials) => {
  return Vue.$http({
    method: 'post',
    url: config_settings.AUTH_SERVER_URL + '/oauth/token',
    data: body_credentials,
    withCredentials: true,
    auth: {
      username: 'USER_CLIENT_APP',
      password: 'password',
    },
  });
};

export const getProfileData = (token) => {
  return Vue.$http({
    method: 'get',
    url: config_settings.RESOURCE_SERVER_URL + '/GET/profile',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const putPassword = (jsonObject) => {
  return Vue.$http({
    method: 'PUT',
    url: config_settings.RESOURCE_SERVER_URL + '/PUT/forget_password',
    data: jsonObject,
  });
};

export const postNewAccount = (jsonObject, origin) => {
  return Vue.$http({
    method: 'POST',
    url: config_settings.RESOURCE_SERVER_URL + '/POST/clients?origin=' + origin,
    data: jsonObject,
  });
};

export const putAccountDataFromSale = (jsonObject, token) => {
  return Vue.$http({
    method: 'PUT',
    url: config_settings.RESOURCE_SERVER_URL + '/PUT/profile-sale',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const postNewAccountToErp = (jsonObject) => {
  return Vue.$http({
    method: 'POST',
    url: 'https://zonasegura.frutaromla.com/frutarom_apitv/api/cliente/registrar',
    data: jsonObject,
  });
};

export const getAccountAddressList = (token) => {
  return Vue.$http({
    method: 'get',
    url: config_settings.RESOURCE_SERVER_URL + '/GET/clients-address',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const postNewClientAddress = (jsonObject, token) => {
  return Vue.$http({
    method: 'POST',
    url: config_settings.RESOURCE_SERVER_URL + '/POST/clients-address',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const putNewClientAddress = (jsonObject, token) => {
  return Vue.$http({
    method: 'PUT',
    url: config_settings.RESOURCE_SERVER_URL + '/PUT/clients-address',
    data: jsonObject,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const deleteClientAddress = (idAddress, token) => {
  return Vue.$http({
    method: 'DELETE',
    url: config_settings.RESOURCE_SERVER_URL + '/DELETE/clients-address/' + idAddress,
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
};

export const postNewSuscription = (jsonObject) => {
  return Vue.$http({
    method: 'POST',
    url: config_settings.RESOURCE_SERVER_URL + '/POST/subscriptions',
    data: jsonObject,
  });
};
