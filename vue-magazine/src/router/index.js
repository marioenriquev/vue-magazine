import Vue from 'vue';
import Router from 'vue-router';

const Page404 = () => import('@/pages/Page404');
const Page500 = () => import('@/pages/Page500');
const Login = () => import('@/pages/Login');
const BookSidebarContainer = () => import('@/components/general/BookSidebarContainer');

Vue.use(Router);

const router = new Router({
  mode: 'history', 
  base: '/revista/',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/magazine',
      name: 'Pages',
      component: {
        render(c) {
          return c('router-view');
        },
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404,
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500,
        },
        {
          path: 'login',
          name: 'Login',
          component: Login,
        },
        {
          path: 'magazine',
          name: 'Magazine',
          component: BookSidebarContainer,
          props: { editorView: false },
        },
        {
          path: 'admin',
          name: 'Admin',
          component: BookSidebarContainer,
          props: { editorView: true },
        },
      ],
    },
    {
      path: '*',
      component: Page404,
    },
  ],
});

router.beforeEach((to, from, next) => {
  let access_token = localStorage.getItem('access_token');
  let refresh_token = localStorage.getItem('refresh_token');
  if (
    to.path != '/login' &&
    to.path != '/magazine' &&
    (access_token == null || refresh_token == null)
  ) {
    next('/login');
  } else {
    next();
  }
});

export default router;
