import { UNAUTHORIZED } from '@/types/httpd.constants';
import Vue from 'vue';

export const useAxiosCustomInterceptor = () => {
  Vue.$http.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      if (error.message === 'Network Error') {
        toastr.error(
          'No se puede conectar con el servidor. Verifique su conexión a internet',
          'ERROR',
          { iconClass: 'customer-danger' },
        );
      } else if (error.response.status === UNAUTHORIZED) {
        if (error.response.data.message === 'Access is denied') {
          toastr.error('Usted no tiene los provilegios para acceder a esta sección', 'ERROR', {
            iconClass: 'customer-danger',
          });
        } else {
          return Promise.reject(error);
        }
      } else {
        return Promise.reject(error);
      }
    },
  );
};
