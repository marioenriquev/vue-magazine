import { config_settings } from '@/config.js';

export const generateTokenBodyRequest = (username, password) => {
  var bodyCredentials = new FormData();
  bodyCredentials.append('grant_type', 'password');
  bodyCredentials.append('username', username);
  bodyCredentials.append('password', password);
  bodyCredentials.append('id_store', config_settings.STORE);
  return bodyCredentials;
};

export const generateRefreshTokenBodyRequest = () => {
  var refreshToken = localStorage.getItem('refresh_token');
  var refreshCredentials = new FormData();
  refreshCredentials.append('grant_type', 'refresh_token');
  refreshCredentials.append('refresh_token', refreshToken);
  refreshCredentials.append('id_store', config_settings.STORE);
  return refreshCredentials;
};

export const deleteTokens = () => {
  localStorage.removeItem('access_token');
  localStorage.removeItem('refresh_token');
  localStorage.removeItem('setOauth2TypeUser');
};

export const setTokens = (token, refreshToken) => {
  localStorage.setItem('access_token', token);
  localStorage.setItem('refresh_token', refreshToken);
};

export const getAccessToken = () => {
  return localStorage.getItem('access_token');
};

export const getRefreshToken = () => {
  return localStorage.getItem('refresh_token');
};
